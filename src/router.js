import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
	mode: 'hash',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			component: () => import('@/views/dashboard/Index'),
			children: [
				// Dashboard
				{
					name: 'dashboard',
					path: '',
					component: () => import('@/views/dashboard/Dashboard')
				},
        {
					name: 'user-type-form',
					path: 'pages/user_type/register',
					component: () => import('@/views/pages/user_type/UserTypeForm'),
				},
        {
					name: 'user-types-list',
					path: 'pages/user_type/index',
					component: () => import('@/views/pages/user_type/UserTypes'),
				},

        {
					name: 'curse-form',
					path: 'pages/curse/register',
					component: () => import('@/views/pages/curse/CurseForm'),
				},
        {
					name: 'curse-list',
					path: 'pages/curse/index',
					component: () => import('@/views/pages/curse/Curses'),
				},

        {
					name: 'user-form',
					path: 'pages/user/register',
					component: () => import('@/views/pages/user/UserForm'),
				},
        {
					name: 'user-list',
					path: 'pages/user/index',
					component: () => import('@/views/pages/user/Users'),
				},

         {
					name: 'inscription-form',
					path: 'pages/inscription/register',
					component: () => import('@/views/pages/inscription/InscriptionForm'),
				},
        {
					name: 'inscription-list',
					path: 'pages/inscription/index',
					component: () => import('@/views/pages/inscription/Inscriptions'),
				},
			]
		}
	]
});

export default router;
