/**
 * User Type Model
 */

export default class UserTypeModel {

  id = null;
  name = "";

  constructor(name) {
    this.name = name;
  }
}
