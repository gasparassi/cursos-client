/**
 * Curse Model
 */

export default class CurseModel {

  id = null;
  name = "";
  description = "";
  value = null;
  date_start_registrations = "";
  date_end_registrations = "";
  max_number_subscribers = null;
  material = "";

  constructor(name, description, value, date_start_registrations, date_end_registrations, max_number_subscribers, material) {
    this.name = name,
    this.description = description,
    this.value = value;
    this.date_start_registrations = date_start_registrations;
    this.date_end_registrations = date_end_registrations;
    this.max_number_subscribers = max_number_subscribers;
    this.material = material;
  }
}
