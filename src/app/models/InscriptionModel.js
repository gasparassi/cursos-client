import UserModel from './UserModel';

/**
 *  Inscription Model
 */

export default class InscriptionModel {

  id = null;
  curse_id = null;
  status = "";
  company = "";
  user = null;

  constructor(curse_id, status, cpf, company, phone, celphone, nome) {
    this.curse_id = curse_id;
    this.status = status;
    this.cpf = cpf;
    this.company = company;
    this.phone = phone;
    this.celphone = celphone;
    this.user = new UserModel();
  }
}
