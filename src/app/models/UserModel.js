import Address from './AddressModel';

/**
 * User Model
 */

export default class UserModel {

  id = null;
  user_type_id = null;
  person_id = null;
  email = "";
  password = "";
  password_confirmation = "";
  address = null;

  constructor(user_type_id, person_id, email, password, password_confirmation) {
    this.user_type_id = user_type_id;
    this.person_id = person_id;
    this.email = email;
    this.password = password;
    this.password_confirmation = password_confirmation;
    this.address = new Address();
  }
}
