/**
 *  Address Model
 */

export default class AddressnModel {

  id = null;
  logradouro = "";
  numero = "";
  complemento = "";
  bairro = "";
  localidade = "";
  uf = "";
  cep = "";
  ddd = "";
  ibge = "";

  constructor(logradouro, numero, complemento, bairro, localidade, uf, cep, ddd, ibge) {
    this.logradouro = logradouro;
    this.numero = numero;
    this.complemento = complemento;
    this.bairro = bairro;
    this.localidade = localidade;
    this.uf = uf;
    this.cep = cep;
    this.ddd = ddd;
    this.ibge = ibge;
  }
}
