import Rest from './Rest'

/**
 * @typedef {UserService}
 */
export default class UserService extends Rest {
    /**
     * @type {String}
     */
    static resource = '/users';

}
