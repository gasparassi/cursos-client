import Rest from './Rest'

/**
 * @typedef {InscriptionService}
 */
export default class InscriptionService extends Rest {
    /**
     * @type {String}
     */
    static resource = '/inscriptions';

    formatDate(fullDate){
      if (!fullDate) return null;
      let date = new Date(fullDate).toISOString().substr(0, 10);
      const[year, month, day] = date.split('-');
      return `${day}.${month}.${year}`;
    }

}
