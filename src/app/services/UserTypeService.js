import Rest from './Rest'

/**
 * @typedef {UserTypeService}
 */
export default class UserTypeService extends Rest {
    /**
     * @type {String}
     */
    static resource = '/user_types';

}
