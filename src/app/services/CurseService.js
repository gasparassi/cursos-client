import Rest from './Rest'

/**
 * @typedef {CurseService}
 */
export default class CurseService extends Rest {
    /**
     * @type {String}
     */
    static resource = '/curses';

    formatDate(fullDate){
      if (!fullDate) return null;
      let date = new Date(fullDate).toISOString().substr(0, 10);
      const[year, month, day] = date.split('-');
      return `${day}.${month}.${year}`;
    }

}
