import InscriptionService from '../services/InscriptionService';
import InscriptionModel from '../models/InscriptionModel';

/**
 * @typedef {InscriptionController}
 */
export default class InscriptionController {

    /**
    * @type {null}
     */

    service = null;

    constructor(){
        this.service = InscriptionService.build();
    }

    inscriptionModel(){
        return new InscriptionModel();
    }

    rules = {
      required: (value) => !!value || "Este campo é obrigatório",
      minCaracterPassword: (value) => (value && value.length >= 6) || "O campo senha precisa ter no mínimo 6 caracteres",
      emailRules: (value) => /.+@.+\..+/.test(value) || "O endereço de e-mail informado é inválido",
    }

    validatorConfirmPassword(password, passwordConfirmation){
    return [
      password === passwordConfirmation || "O campo senha e confirme sua senha não são iguais",
    ];
  }


    register( inscription ) {
        return this.service.create(inscription).then(
            response => {
                return response;
            }
        );
    }

    getAll() {
        return this.service.search(null).then(
            response => {
                return response.rows;
            }
        );
    }

    getOne(id){
        return this.service.read(id).then(
            response => {
                return response;
            }
        );
    }

    update( inscription ) {
       return this.service.update(inscription).then(
            response => {
                return response;
            }
        );
    }

    delete( inscription ) {
      return this.service.destroy(inscription).then(
            response => {
                return response;
            }
        );
    }

}
