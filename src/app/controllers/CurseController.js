import CurseService from '../services/CurseService';
import CurseModel from '../models/CurseModel';

/**
 * @typedef {UserTypeController}
 */
export default class CurseController {

    /**
    * @type {null}
     */

    service = null;

    constructor(){
        this.service = CurseService.build();
    }

    curseModel(){
        return new CurseModel();
    }

    rules = {
    required: (value) => !!value || "Este campo é obrigatório",
  }


    register( curse ) {
        return this.service.create(curse).then(
            response => {
                return response;
            }
        );
    }

    getAll() {
        return this.service.search(null).then(
            response => {
                return response.rows;
            }
        );
    }

    update( curse ) {
       return this.service.update(curse).then(
            response => {
                return response;
            }
        );
    }

    delete( curse ) {
      return this.service.destroy(curse).then(
            response => {
                return response;
            }
        );
    }

}
