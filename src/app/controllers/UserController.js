import UserService from '../services/UserService';
import UserModel from '../models/UserModel';

/**
 * @typedef {UserTypeController}
 */
export default class UserController {

    /**
    * @type {null}
     */

    service = null;

    constructor(){
        this.service = UserService.build();
    }

    userModel(){
        return new UserModel();
    }

    rules = {
    required: (value) => !!value || "Este campo é obrigatório",
    minCaracterPassword: (value) => (value && value.length >= 6) || "O campo senha precisa ter no mínimo 6 caracteres",
    emailRules: (value) => /.+@.+\..+/.test(value) || "O endereço de e-mail informado é inválido",
  }

  maskTel(value){
      if (!! value) {
        return value.length == 15 ? '(##) #####-####' : '(##) ####-####';
      } else{
        return '(##) #####-####';
      }
  }

  validatorConfirmPassword(password, passwordConfirmation){
    return [
      password === passwordConfirmation || "O campo senha e confirme sua senha não são iguais",
    ];
  }


    register( user ) {
        return this.service.create(user).then(
            response => {
                return response;
            }
        );
    }

    getAll() {
        return this.service.search(null).then(
            response => {
                return response.rows;
            }
        );
    }

    update( user ) {
       return this.service.update(user).then(
            response => {
                return response;
            }
        );
    }

    delete( user ) {
      return this.service.destroy(user).then(
            response => {
                return response;
            }
        );
    }

}
