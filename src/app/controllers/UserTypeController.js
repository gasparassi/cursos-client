import UserTypeService from '../services/UserTypeService';
import UserTypeModel from '../models/UserTypeModel';

/**
 * @typedef {UserTypeController}
 */
export default class UserTypeController {

    /**
    * @type {null}
     */

    service = null;

    constructor(){
        this.service = UserTypeService.build();
    }

    user_typeModel(){
        return new UserTypeModel();
    }

    rules = {
    required: (value) => !!value || "Este campo é obrigatório",
  }


    register( user_type ) {
        return this.service.create(user_type).then(
            response => {
                return response;
            }
        );
    }

    getAll() {
        return this.service.search(null).then(
            response => {
                return response.rows;
            }
        );
    }

    update( user_type ) {
       return this.service.update(user_type).then(
            response => {
                return response;
            }
        );
    }

    delete( user_type ) {
      return this.service.destroy(user_type).then(
            response => {
                return response;
            }
        );
    }

}
